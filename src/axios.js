import axios from 'axios'

const apiClient = axios.create({
    baseURL: `http://localhost:3000`,
    timeout: 2000,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    },
})

apiClient.interceptors.response.use(function (response) {
    return response.data;
}, function (error) {
    return Promise.reject(error);
});
export default {
    getEmployees() {
        return apiClient.get('/employees')
    },
    getSales() {
        return apiClient.get('/sales')
    },
    getStatistics() {
        return apiClient.get('/statistics')
    },
    getTimeline() {
        return apiClient.get('/timeline')
    },
}